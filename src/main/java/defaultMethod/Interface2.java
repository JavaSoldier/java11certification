package defaultMethod;

public interface Interface2 {
    void print();
    default void printDef(){
        System.out.println("Interface2 print me!");
    }
}
