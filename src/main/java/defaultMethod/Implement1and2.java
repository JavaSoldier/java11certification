package defaultMethod;

public class Implement1and2 implements Interface1, Interface2 {
    
    @Override
    public void print() {
        System.out.println("Implement1and2 print!");
    }

    @Override
    public void printDef() {
        System.out.println("Implement1and2 print default!");
    }

    public void superAPrintDef() {
        Interface1.super.printDef();
    }
}
