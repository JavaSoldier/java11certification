package defaultMethod;

public class Main {
    
    public static void main(String[] args) {
        Whoami whoami = new Whoami();
        whoami.print();
        whoami.printDef();
        whoami.superAPrintDef();
    }
}
