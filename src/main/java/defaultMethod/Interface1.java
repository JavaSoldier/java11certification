package defaultMethod;

public interface Interface1 {
    void print();
    default void printDef(){
        System.out.println("Interface1 print me!");
    }
}
