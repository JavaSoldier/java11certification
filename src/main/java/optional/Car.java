package optional;

public class Car {
    public String model;
    public Insurance insurance;

    public Car(String model, Insurance insurance) {
        this.model = model;
        this.insurance = insurance;
    }

    public String getModel() {
        return model;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                '}';
    }
}