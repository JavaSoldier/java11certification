package optional;

public class UserService {

    public User getUser(Long id) {
        if (Long.valueOf(1L).equals(id)) {
            return new User("First");
        } else return null;
    }
}
