package optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicReference;

import static java.util.Optional.ofNullable;

public class FunctionalVsOldSchool {

    private static final Logger LOG = LogManager.getLogger();

    public UserService userService;

    public FunctionalVsOldSchool(UserService userService) {
        this.userService = userService;
    }

    public String getUserOrThrowEx(Long id) {
        User usr = userService.getUser(id);
        if (usr != null) {
            return usr.name;
        } else {
            throw new IllegalArgumentException();
        }
    }

    //less code but bad to debug and less readable
    public String getUserOrThrowExOptional(Long id) {
        return ofNullable(userService.getUser(id)).orElseThrow(IllegalArgumentException::new).getName();
    }

    public String getUserOrReturnDefault(Long id) {
        User user = userService.getUser(id);
        if (user != null) {
            return user.getName();
        } else return "defaultName";
    }

    public String getUserOrReturnDefaultOptional(Long id) {
        return ofNullable(userService.getUser(id)).map(User::getName).orElse("defaultName");
    }

    //not bad - readable and clear! what is wrong with null check?
    public String getInsurance(User user) {
        if (user != null && user.getCar() != null && user.getCar().getInsurance() != null) {
            return user.getCar().getInsurance().getName();
        } else return "no insurance";
    }

    //shitty! where is profit of using this functional programming? brain fuck!!!
    public String getInsuranceOptional(User user) {
        return ofNullable(user)
                .map(User::getCar)
                .map(Car::getInsurance)
                .map(Insurance::getName)
                .orElse("no insurance");
    }

    public String getInsuranceComplexLogic(User user) {
        String result = "no insurance";
        if (user != null) {
            if (user.getCar() != null) {
                Car car = user.getCar();
                LOG.info("user {} has car {} ", user, car);
                if (car.getInsurance() != null) {
                    Insurance insurance = car.getInsurance();
                    LOG.info("car {} has insurance {}", car, insurance);
                    return insurance.getName();
                } else {
                    LOG.info("no insurance for car {} user {} pay the bill", car, user);
                }
            } else {
                LOG.info("user {} has no car", user);
                result = "no car";
            }
        } else {
            LOG.error("user is null!");
            result = "no user";
        }
        return result;
    }

    //functional style 4 getInsuranceComplexLogic is worse than old
    public String getInsuranceComplexLogicFunctional(User user) {
        AtomicReference<String> result = new AtomicReference<>("no user");
        return ofNullable(user)
                // LOG.error("user is null!"); how log if user is null and execution goes to .orElse
                .map(usr -> {
                    Car car = usr.getCar();
                    if (car != null) {
                        LOG.info("user {} has car {} ", user, car);
                    } else {
                        LOG.info("user {} has no car", user);
                        result.set("no car");
                    }
                    return usr.getCar();
                })
                .map(car -> {
                    Insurance insurance = car.getInsurance();
                    if (insurance != null) {
                        LOG.info("car {} has insurance {}", car, insurance);
                    } else {
                        LOG.info("no insurance for car {} user {} pay the bill", car, user);
                        result.set("no insurance");
                    }
                    return car.getInsurance();
                })
                .map(Insurance::getName)
                .orElse(result.get());
    }
}
