package optional;

public class Insurance {
    public String name;

    public String getName() {
        return name;
    }

    public Insurance(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Insurance{" +
                "name='" + name + '\'' +
                '}';
    }
}