package optional;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FunctionalVsOldSchoolTest {

    FunctionalVsOldSchool functionalVsOldSchool = new FunctionalVsOldSchool(new UserService());

    @Test
    public void getUserOrThrowEx() {
        assertNotNull(functionalVsOldSchool.getUserOrThrowEx(1L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserOrThrowExWrongId() {
        functionalVsOldSchool.getUserOrThrowEx(2L);
    }
    
    @Test
    public void getUserOrThrowExOptional() {
        assertNotNull(functionalVsOldSchool.getUserOrThrowExOptional(1L));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getUserOrThrowExOptionalWrongId() {
        functionalVsOldSchool.getUserOrThrowExOptional(2L);
    }

    @Test
    public void getUserOrReturnDefaultWrongId() {
        assertEquals("defaultName", functionalVsOldSchool.getUserOrReturnDefault(2L));
    }

    @Test
    public void getUserOrReturnDefaultOptionalWrongId() {
        assertEquals("defaultName", functionalVsOldSchool.getUserOrReturnDefaultOptional(2L));

    }

    @Test
    public void getInsurance() {
        Insurance insurance = new Insurance("TAC");
        Car car = new Car("lanos", insurance);
        User user = new User("name", car);
        assertEquals("TAC", functionalVsOldSchool.getInsurance(user));
    }

    @Test
    public void getInsuranceNoUser() {
        assertEquals("no insurance", functionalVsOldSchool.getInsurance(null));
    }

    @Test
    public void getInsuranceNoCar() {
        User user = new User("name", null);
        assertEquals("no insurance", functionalVsOldSchool.getInsurance(user));
    }

    @Test
    public void getInsuranceNoInsurance() {
        Car car = new Car("lanos", null);
        User user = new User("name", car);
        assertEquals("no insurance", functionalVsOldSchool.getInsurance(user));
    }

    @Test
    public void getInsuranceOptionalNoUser() {
        assertEquals("no insurance", functionalVsOldSchool.getInsuranceOptional(null));
    }

    @Test
    public void getInsuranceOptionalNoCar() {
        User user = new User("name", null);
        assertEquals("no insurance", functionalVsOldSchool.getInsuranceOptional(user));
    }

    @Test
    public void getInsuranceComplexLogic() {
        Insurance insurance = new Insurance("TAC");
        Car car = new Car("lanos", insurance);
        User user = new User("name", car);
        assertEquals("TAC", functionalVsOldSchool.getInsuranceComplexLogic(user));
    }

    @Test
    public void getInsuranceOptionalNoInsurance() {
        Car car = new Car("lanos", null);
        User user = new User("name", car);
        assertEquals("no insurance", functionalVsOldSchool.getInsuranceOptional(user));
    }

    @Test
    public void getInsuranceComplexLogicNoUser() {
        assertEquals("no user", functionalVsOldSchool.getInsuranceComplexLogic(null));
    }

    @Test
    public void getInsuranceComplexLogicNoCar() {
        assertEquals("no car", functionalVsOldSchool.getInsuranceComplexLogic(new User(null)));
    }

    @Test
    public void getInsuranceComplexLogicFunctional() {
        Insurance insurance = new Insurance("TAC");
        Car car = new Car("lanos", insurance);
        User user = new User("name", car);
        assertEquals("TAC", functionalVsOldSchool.getInsuranceComplexLogicFunctional(user));
    }

    @Test
    public void getInsuranceComplexLogicFunctionalNoUser() {
//        assertEquals("no user", functionalVsOldSchool.getInsuranceComplexLogicFunctional(null)); todo decide how2 fix
    }

    @Test
    public void getInsuranceComplexLogicFunctionalNoCar() {
        assertEquals("no car", functionalVsOldSchool.getInsuranceComplexLogicFunctional(new User(null)));
    }

    @Test
    public void getInsuranceComplexLogicFunctionalNoInsurance() {
        Car car = new Car("lanos", null);
        User user = new User("name", car);
        assertEquals("no insurance", functionalVsOldSchool.getInsuranceComplexLogicFunctional(user));
    }
}
